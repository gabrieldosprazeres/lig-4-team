## Projeto Lig-4

# Regra geral
    Criação de estilização no CSS para ser agregada via DOM
    MOBILE FIRST

# 0.5 Adicionar explicação no HTML (Regras do Jogo)

# 1. Criar tabela via DOM
    1.1.1 - Criar um Array representando a tabela com 7 Arrays(vazios) dentro representando as colunas 
    1.1.2 - Criar variável para receber uma string `table` e criar a tabela (Usando for aninhado)

# 2. Criar dois discos
    2.1 - Criar dois discos com cores diferentes
    2.2 - Cada disco receberá uma classe diferente

# 3. Criar uma função para adicionar elementos em um array
    3.1 - A função adicionará um elemento à um array vazio após a coluna ser escolhida (clicada)
    3.2 - Para ativação no HTML, será utilizado uma "Seta" para identificar a tabela a ser escolhida
    3.3 - Empilhe discos vermelhos e pretos em uma coluna usando um layout de caixa flex

# 4. Criar condição de Vitória ou Empate
    4.1 Se todas as colunas estiverem preenchidas = EMPATE
    4.2 Se tiver quatro discos da mesma cor alinhados = VITÓRIA
    4.3 Criar um contador de virória (Vermelho/Preto)

# ?. Criar uma função contador
    ?.1 - Criar um contador para condicionar as cores Preta e Vermelha a serem ativadas por impar ou par

# ?. Chamar a função final no Evento de Clique
    ?.1 - A função será criada com addEventListener
    
# Condicionais
    1. Após encher uma coluna (6 discos), não permita que mais discos sejam adicionados
    2. Verifique se o último disco adicionado completou uma linha de quatro peças na coluna (verticalmente)
    3. Verifique se o último disco adicionado completou uma linha de quatro peças horizontalmente
    4. Verifique se o último disco adicionado completou uma linha de quatro peças em uma diagonal descendente ou ascendente

# Ideias
    1. Identificar cada cor com um valor (Vermelho = 1 / Preto = -1)