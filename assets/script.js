// Criando variaveil do Body
const body = document.getElementById("body");
// Criando elementos do HTML
const sectionTabela = document.createElement("section");
sectionTabela.id = "sectionTabela";
body.appendChild(sectionTabela);

const divContainer = document.createElement("div");
divContainer.className = "container";
sectionTabela.appendChild(divContainer);

const footer = document.createElement("footer");
const link = document.createElement("a");
footer.id = "footer";
body.appendChild(footer);
footer.innerHTML =
  '<p>Projeto desenvolvido por:</p> <a target="_blank" href="https://gitlab.com/gabrieldosprazeres/lig-4-team">quartERROR 404</a>';

const divSeta = document.createElement("div");
divSeta.id = "divSeta";
divContainer.appendChild(divSeta);
for (let i = 0; i <= 6; i++) {
  const span = document.createElement("span");
  span.setAttribute("id", `span${i}`);
  span.setAttribute("class", "spanSeta");
  span.innerHTML = "<i class='fas fa-angle-double-down fa-2x'></i>";
  divSeta.appendChild(span);
}

const divTabela = document.createElement("div");
divTabela.className = "divTabela";
divContainer.appendChild(divTabela);
for (let i = 0; i <= 6; i++) {
  const div = document.createElement("div");
  div.setAttribute("id", `div${i}`);
  div.setAttribute("class", "divTable");
  divTabela.appendChild(div);
}

const divButton = document.createElement("div");
divButton.className = "divButton";
divContainer.appendChild(divButton);

const button = document.createElement("button");
button.innerText = "Reset";
button.id = "reset";
divButton.appendChild(button);

// Criando variaveis do HTML
const div0 = document.getElementById("div0");
const div1 = document.getElementById("div1");
const div2 = document.getElementById("div2");
const div3 = document.getElementById("div3");
const div4 = document.getElementById("div4");
const div5 = document.getElementById("div5");
const div6 = document.getElementById("div6");
const reset = document.getElementById("reset");
const setaClick0 = document.getElementsByClassName(
  "fas fa-angle-double-down fa-2x"
)[0];
const setaClick1 = document.getElementsByClassName(
  "fas fa-angle-double-down fa-2x"
)[1];
const setaClick2 = document.getElementsByClassName(
  "fas fa-angle-double-down fa-2x"
)[2];
const setaClick3 = document.getElementsByClassName(
  "fas fa-angle-double-down fa-2x"
)[3];
const setaClick4 = document.getElementsByClassName(
  "fas fa-angle-double-down fa-2x"
)[4];
const setaClick5 = document.getElementsByClassName(
  "fas fa-angle-double-down fa-2x"
)[5];
const setaClick6 = document.getElementsByClassName(
  "fas fa-angle-double-down fa-2x"
)[6];

const discoPreto = document.createElement("div");
discoPreto.className = "discoPreto";
const discoVermelho = document.createElement("div");
discoVermelho.className = "discoVermelho";

let discArr = [[], [], [], [], [], [], []];

let discGameplay = [
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
];

// Funções
const popUpVictory = (verificarBola) => {
  const popUp = document.createElement("div");
  const localPopUp = document.getElementsByClassName("regras")[0];
  popUp.id = "victory";
  localPopUp.appendChild(popUp);
  if (verificarBola === -1) {
    return (popUp.innerText = `Parabéns Preto, você foi o vencedor!!`);
  } else {
    return (popUp.innerText = `Parabéns Vermelho, você foi o vencedor!!`);
  }
};

const popUpDraw = () => {
  const popUp = document.createElement("div");
  const localPopUp = document.getElementsByClassName("regras")[0];
  popUp.id = "draw";
  localPopUp.appendChild(popUp);
  return (popUp.innerText = `Infelizmente não houve vencedores..`);
};

let count = 0;
const contadorParaBolas = () => {
  count += 1;
  if (count % 2 === 0) {
    return 1;
  }
  return -1;
};

const verificarVitóriaOuEmpate = (verificarBola) => {
  const maxX = 4;
  const maxY = 3;

  for (let i = 0; i < discGameplay.length; i++) {
    for (let b = 0; b < maxY; b++) {
      if (discGameplay[i][b] !== 0) {
        if (
          discGameplay[i][b] === discGameplay[i][b + 1] &&
          discGameplay[i][b] === discGameplay[i][b + 2] &&
          discGameplay[i][b] === discGameplay[i][b + 3]
        ) {
          popUpVictory(verificarBola);
          return true;
        }
      }
    }
  }

  for (let i = 0; i < maxX; i++) {
    for (let b = 0; b < discGameplay[0].length; b++) {
      if (discGameplay[i][b] !== 0) {
        if (
          discGameplay[i][b] === discGameplay[i + 1][b] &&
          discGameplay[i][b] === discGameplay[i + 2][b] &&
          discGameplay[i + 3][b] === discGameplay[i][b]
        ) {
          popUpVictory(verificarBola);
          return true;
        }
      }
    }
  }

  for (let i = 0; i < maxX; i++) {
    for (let b = 0; b < maxY; b++) {
      if (discGameplay[i][b] !== 0) {
        if (
          discGameplay[i][b] === discGameplay[i + 1][b + 1] &&
          discGameplay[i][b] === discGameplay[i + 2][b + 2] &&
          discGameplay[i][b] === discGameplay[i + 3][b + 3]
        ) {
          popUpVictory(verificarBola);
          return true;
        }
      }
    }
  }

  for (let i = 0; i < maxX; i++) {
    for (let b = 0; b < maxY; b++) {
      if (discGameplay[i][b + 3] !== 0) {
        if (
          discGameplay[i][b + 3] === discGameplay[i + 1][b + 2] &&
          discGameplay[i][b + 3] === discGameplay[i + 2][b + 1] &&
          discGameplay[i][b + 3] === discGameplay[i + 3][b]
        ) {
          popUpVictory(verificarBola);
          return true;
        }
      }
    }
  }

  if (
    discArr[0].length &&
    discArr[1].length &&
    discArr[2].length &&
    discArr[3].length &&
    discArr[4].length &&
    discArr[5].length &&
    discArr[6].length === 6
  ) {
    popUpDraw();
    return "Empate";
  }

  return false;
};

const prohibitingClick = () => {
  for (let i = 0; i < discArr.length; i++) {
    if (discArr[i].length === 6) {
      const forbiddenClick = document.getElementById(`span${i}`);
      forbiddenClick.style.pointerEvents = "none";
      forbiddenClick.style.opacity = 0.2;
    }
  }
};

const releasingClick = () => {
  for (let i = 0; i < discArr.length; i++) {
    const releasedClick = document.getElementById(`span${i}`);
    releasedClick.style.pointerEvents = "inherit";
    releasedClick.style.opacity = 1;
  }
};

const adicionandoElementosNoHTML = (ev) => {
  let verificarBola = contadorParaBolas();
  const vitoria = verificarVitóriaOuEmpate(verificarBola);
  verificarBola;
  let bola = [];
  if (verificarBola === 1) {
    const discoVermelho = document.createElement("div");
    discoVermelho.className = "discoVermelho";
    bola.push(discoVermelho);
  } else if (verificarBola === -1) {
    const discoPreto = document.createElement("div");
    discoPreto.className = "discoPreto";
    bola.push(discoPreto);
  }

  if (vitoria === false) {
    if (ev.target === setaClick0 && discArr[0].length < 6) {
      discArr[0].push(verificarBola);
      div0.appendChild(bola[0]);
      let elementCount0 = div0.childElementCount - 1;
      discGameplay[0][elementCount0] = verificarBola;
      bola.pop();
    } else if (ev.target === setaClick1 && discArr[1].length < 6) {
      discArr[1].push(verificarBola);
      div1.appendChild(bola[0]);
      let elementCount1 = div1.childElementCount - 1;
      discGameplay[1][elementCount1] = verificarBola;
      bola.pop();
    } else if (ev.target === setaClick2 && discArr[2].length < 6) {
      discArr[2].push(verificarBola);
      div2.appendChild(bola[0]);
      let elementCount2 = div2.childElementCount - 1;
      discGameplay[2][elementCount2] = verificarBola;
      bola.pop();
    } else if (ev.target === setaClick3 && discArr[3].length < 6) {
      discArr[3].push(verificarBola);
      div3.appendChild(bola[0]);
      let elementCount3 = div3.childElementCount - 1;
      discGameplay[3][elementCount3] = verificarBola;
      bola.pop();
    } else if (ev.target === setaClick4 && discArr[4].length < 6) {
      discArr[4].push(verificarBola);
      div4.appendChild(bola[0]);
      let elementCount4 = div4.childElementCount - 1;
      discGameplay[4][elementCount4] = verificarBola;
      bola.pop();
    } else if (ev.target === setaClick5 && discArr[5].length < 6) {
      discArr[5].push(verificarBola);
      div5.appendChild(bola[0]);
      let elementCount5 = div5.childElementCount - 1;
      discGameplay[5][elementCount5] = verificarBola;
      bola.pop();
    } else if (ev.target === setaClick6 && discArr[6].length < 6) {
      div6.appendChild(bola[0]);
      discArr[6].push(verificarBola);
      let elementCount6 = div6.childElementCount - 1;
      discGameplay[6][elementCount6] = verificarBola;
      bola.pop();
    }
  }
  prohibitingClick();
  if (verificarVitóriaOuEmpate(verificarBola)) {
    for (let i = 0; i < discArr.length; i++) {
      const forbiddenClick = document.getElementById(`span${i}`);
      forbiddenClick.style.pointerEvents = "none";
      forbiddenClick.style.opacity = 0.2;
    }
  }
};

setaClick0.addEventListener("click", adicionandoElementosNoHTML);
setaClick1.addEventListener("click", adicionandoElementosNoHTML);
setaClick2.addEventListener("click", adicionandoElementosNoHTML);
setaClick3.addEventListener("click", adicionandoElementosNoHTML);
setaClick4.addEventListener("click", adicionandoElementosNoHTML);
setaClick5.addEventListener("click", adicionandoElementosNoHTML);
setaClick6.addEventListener("click", adicionandoElementosNoHTML);
button.addEventListener("click", () => {
  releasingClick();

  if (
    discArr[0].length &&
    discArr[1].length &&
    discArr[2].length &&
    discArr[3].length &&
    discArr[4].length &&
    discArr[5].length &&
    discArr[6].length === 6
  ) {
    const localPopUpDraw = document.getElementsByClassName("regras")[0];
    const removeDraw = document.getElementById("draw");
    localPopUpDraw.removeChild(removeDraw);
  } else {
    const localPopUpVictory = document.getElementsByClassName("regras")[0];
    const removeVictory = document.getElementById("victory");
    localPopUpVictory.removeChild(removeVictory);
  }

  discArr = [[], [], [], [], [], [], []];

  discGameplay = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
  ];

  div0.innerText = "";
  div1.innerText = "";
  div2.innerText = "";
  div3.innerText = "";
  div4.innerText = "";
  div5.innerText = "";
  div6.innerText = "";

  count = 0;
});
